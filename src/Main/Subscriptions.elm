module Main.Subscriptions exposing (subscriptions)

import Main.Actions exposing (Action(..))
import Main.Model exposing (Model, ScanStatus(..))
import Time
import Utils.Subscriptions as Subscriptions


subscriptions : Model -> Sub Action
subscriptions model =
    Sub.batch
        [ Subscriptions.keypress Keypress
        , case model.status of
            Scanning scanProgress pauseStatus ->
                Sub.batch[Time.every 100 (CheckTimer scanProgress pauseStatus)]

            NotStartedScanning ->
                Sub.none

            CountingDown _ ->
                Sub.none

            FinishedScanning ->
                Sub.none
        ]
