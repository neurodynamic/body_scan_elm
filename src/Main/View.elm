module Main.View exposing (view)

import Element exposing (..)
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Main.Actions exposing (Action(..))
import Main.Model as Model
    exposing
        ( Direction(..)
        , Model
        , PauseStatus(..)
        , ScanProgress
        , ScanStatus(..)
        )
import Time
import Utils.Time as TimeUtils


view : Model -> Html Action
view model =
    layout [ padding 40 ]
        (column
            [ centerX
            , Font.center
            , spacing 50
            ]
            [ paragraph [ Region.heading 1, Font.size 30 ] [ text "Guided Body Scan Meditation" ]
            , row
                [ centerX
                , spacing 40
                ]
                [ directionSelector model.direction
                , durationInput model.duration
                ]
            , controlButtons model.status
            , infoDisplay model
            ]
        )


directionSelector : Direction -> Element Action
directionSelector currentDirection =
    el [ centerX ]
        (Input.radio
            [ padding 10
            , spacing 5
            ]
            { onChange = SelectDirection
            , selected = Just currentDirection
            , label = Input.labelAbove [] (text "Scan Direction")
            , options =
                [ Input.option FeetToHead (text "Feet to Head")
                , Input.option HeadToFeet (text "Head to Feet")
                ]
            }
        )


durationInput : String -> Element Action
durationInput currentDuration =
    column
        [ centerX
        , alignTop
        , spacing 10
        ]
        [ Input.text [ htmlAttribute (Html.Attributes.type_ "number"), width (px 100), centerX ]
            { onChange = SetDuration
            , text = currentDuration
            , placeholder = Nothing
            , label =
                Input.labelAbove [ paddingEach { top = 0, bottom = 5, left = 0, right = 0 } ]
                    (text "Scan Duration (minutes)")
            }
        ]


controlButtons : ScanStatus -> Element Action
controlButtons status =
    let
        buttonFunc =
            Input.button
                [ Border.width 3
                , Border.rounded 5
                , padding 5
                , width (minimum 160 fill)
                ]
    in
    case status of
        NotStartedScanning ->
            buttonFunc
                { onPress = Just (RunScanCountdown 5)
                , label = text "Start Scan"
                }

        CountingDown _ ->
            buttonFunc
                { onPress = Just StopScan
                , label = text "Stop Scan"
                }

        Scanning scanProgress Running ->
            row [ width fill, spacing 10 ]
                [ buttonFunc
                    { onPress = Just (PauseScan scanProgress)
                    , label = text "Pause Scan"
                    }
                , buttonFunc
                    { onPress = Just StopScan
                    , label = text "Stop Scan"
                    }
                ]

        Scanning scanProgress Paused ->
            row [ width fill, spacing 10 ]
                [ buttonFunc
                    { onPress = Just (UnpauseScan scanProgress)
                    , label = text "Unpause Scan"
                    }
                , buttonFunc
                    { onPress = Just StopScan
                    , label = text "Stop Scan"
                    }
                ]

        FinishedScanning ->
            buttonFunc
                { onPress = Just (RunScanCountdown 5)
                , label = text "Start New Scan"
                }


infoDisplay : Model -> Element Action
infoDisplay model =
    column [ Font.center, centerX, spacing 20 ]
        [ case model.status of
            NotStartedScanning ->
                basicInstructions

            CountingDown int ->
                paragraph [ Font.size 30 ]
                    [ text (String.fromInt int)
                    , sleepDisablingCustomElement
                    ]

            Scanning scanProgress status ->
                column []
                    [ scanStatus scanProgress model
                    , case status of
                        Running ->
                            sleepDisablingCustomElement

                        Paused ->
                            none
                    ]

            FinishedScanning ->
                column []
                    [ paragraph [] [ text "Finished scanning" ]
                    , sourceCodeLink
                    ]
        ]


sleepDisablingCustomElement : Element action
sleepDisablingCustomElement =
    html (Html.node "mil-no-sleep" [] [])


basicInstructions : Element Action
basicInstructions =
    column [ spacing 20 ]
        [ paragraph [ width (maximum 400 fill) ] [ text "Press \"Start Scan\" to start a new guided body scan meditation." ]
        , paragraph [ width (maximum 400 fill) ] [ text "If your computer/phone has a speaking voice, it will tell you what parts of your body to focus on one after the other." ]
        , paragraph [ width (maximum 400 fill) ] [ text "If not, the current body part to focus on will also show up on the page." ]
        , paragraph [ width (maximum 400 fill) ] [ text "If you're using this site on your phone, you may need to wear headphones for the spoken instructions to work." ]
        , sourceCodeLink
        ]


scanStatus : ScanProgress -> Model -> Element Action
scanStatus { startTime, timeAtLastTick } model =
    let
        completeSoFar =
            TimeUtils.differenceToTimeString startTime timeAtLastTick

        totalTime =
            TimeUtils.millisToTimeString (Model.scanDurationInMilliseconds model)
    in
    column [ spacing 20 ]
        [ paragraph [] [ text (completeSoFar ++ " of " ++ totalTime ++ " minutes complete") ]
        , paragraph [] [ text ("Focus: " ++ Model.currentPart model) ]
        ]


sourceCodeLink : Element Action
sourceCodeLink =
    newTabLink
        [ Font.color (rgba 0 0 1 0.5)
        , width (px 220)
        , centerX
        , paddingEach { top = 40, bottom = 0, left = 0, right = 0 }
        , mouseOver [ Font.color (rgba 0 0 1 0.7) ]
        ]
        { url = "https://gitlab.com/neurodynamic/body_scan_elm"
        , label =
            paragraph
                []
                [ text "click here to see the source code for this app"
                ]
        }
