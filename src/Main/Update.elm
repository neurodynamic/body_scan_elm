module Main.Update exposing (update)

import Main.Actions exposing (Action(..))
import Main.Model as Model exposing (Direction(..), Model, PauseStatus(..), ScanProgress, ScanStatus(..))
import Main.Ports as Ports
import Process
import Task
import Time
import Utils.List as ListUtils
import Utils.Phrases as Phrases
import Utils.Time as TimeUtils


update : Action -> Model -> ( Model, Cmd Action )
update action model =
    case action of
        SetDuration minutes ->
            ( { model | duration = minutes }, Cmd.none )

        SelectDirection direction ->
            ( { model | direction = direction }, Cmd.none )

        RunScanCountdown count ->
            runScanCountdown count model

        StartScan time ->
            let
                newModel =
                    { model | status = Scanning { startTime = time, timeAtLastTick = time } Running }

                firstPart =
                    Model.currentPart newModel
            in
            ( newModel
            , Cmd.batch
                [ Ports.preventSleeping ()
                , Ports.say ("Focus on your " ++ firstPart)
                ]
            )

        CheckTimer scanProgress pauseStatus currentTime ->
            if pauseStatus == Paused then
                let
                    {- Advance start time so that you're still at the same
                       progress point when you unpause.
                       This is a little weird, but it's the only way to do it
                       if we're calculating how far along we are as a function
                       of current time minus start time.
                    -}
                    newStartTime =
                        TimeUtils.difference scanProgress.timeAtLastTick currentTime
                            |> TimeUtils.add scanProgress.startTime
                in
                ( { model | status = Scanning { startTime = newStartTime, timeAtLastTick = currentTime } pauseStatus }, Cmd.none )

            else if Model.justFinishedAPart scanProgress currentTime model then
                sayNextInstruction scanProgress.startTime currentTime pauseStatus model

            else
                ( { model | status = Scanning { startTime = scanProgress.startTime, timeAtLastTick = currentTime } pauseStatus }, Cmd.none )

        PauseScan scanProgress ->
            pauseScan scanProgress model

        UnpauseScan scanProgress ->
            unpauseScan scanProgress model

        StopScan ->
            stopScan model

        Keypress key ->
            case key of
                " " ->
                    case model.status of
                        NotStartedScanning ->
                            runScanCountdown 5 model

                        CountingDown _ ->
                            stopScan model

                        Scanning scanProgress Running ->
                            pauseScan scanProgress model

                        Scanning scanProgress Paused ->
                            unpauseScan scanProgress model

                        FinishedScanning ->
                            runScanCountdown 5 model
                "Escape" ->
                            stopScan model
                "Delete" ->
                            stopScan model

                _ ->
                    ( model, Cmd.none )


runScanCountdown : Int -> Model -> ( Model, Cmd Action )
runScanCountdown count model =
    if scanHasBeenCanceled count model.status then
        ( model, Ports.allowSleeping () )

    else if count < 1 then
        ( model, Task.perform StartScan Time.now )

    else
        ( { model | status = CountingDown count }
        , Cmd.batch
            [ Ports.say (String.fromInt count)
            , Task.perform RunScanCountdown (countDownByOneInOneSecond count)
            ]
        )


stopScan : Model -> ( Model, Cmd Action )
stopScan model =
    ( { model | status = NotStartedScanning }
    , Ports.allowSleeping ()
    )


pauseScan : ScanProgress -> Model -> ( Model, Cmd Action )
pauseScan scanProgress model =
    ( { model | status = Scanning scanProgress Paused }
    , Ports.allowSleeping ()
    )


unpauseScan : ScanProgress -> Model -> ( Model, Cmd Action )
unpauseScan scanProgress model =
    ( { model | status = Scanning scanProgress Running }
    , Ports.preventSleeping ()
    )


sayNextInstruction : Time.Posix -> Time.Posix -> PauseStatus -> Model -> ( Model, Cmd Action )
sayNextInstruction startTime currentTime pauseStatus model =
    let
        newIndex =
            Model.getCurrentPartIndex startTime currentTime model

        newModel =
            { model | status = Scanning { startTime = startTime, timeAtLastTick = currentTime } pauseStatus }
    in
    if Model.scanComplete newIndex newModel then
        ( { model | status = FinishedScanning }
        , Cmd.batch
            [ Ports.say "Scan complete!"
            , Ports.allowSleeping ()
            ]
        )

    else
        sayFocusOnNextPart newIndex newModel


sayFocusOnNextPart : Int -> Model -> ( Model, Cmd Action )
sayFocusOnNextPart newIndex model =
    let
        part =
            Model.currentPart model
    in
    ( model
    , Ports.say (partFocusInstruction part newIndex)
    )


partFocusInstruction : String -> Int -> String
partFocusInstruction part index =
    let
        focusLineIndex =
            modBy (List.length Phrases.focusLinesWithYour) index

        focusLine =
            (if String.contains "your" part then
                Phrases.focusLinesWithThe

             else
                Phrases.focusLinesWithYour
            )
                |> ListUtils.getByIndex focusLineIndex
                |> Maybe.withDefault "Focus on your"
    in
    focusLine ++ " " ++ part


countDownByOneInOneSecond : Int -> Task.Task x Int
countDownByOneInOneSecond int =
    Process.sleep 1000
        |> Task.andThen (\_ -> Task.succeed (int - 1))


scanHasBeenCanceled : Int -> ScanStatus -> Bool
scanHasBeenCanceled countdownInt status =
    countdownInt /= 5 && status == NotStartedScanning
