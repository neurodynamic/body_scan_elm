port module Main.Ports exposing (allowSleeping, preventSleeping, say)


port say : String -> Cmd action


port preventSleeping : () -> Cmd action


port allowSleeping : () -> Cmd action
