module Main.Model exposing
    ( Direction(..)
    , Model
    , PauseStatus(..)
    , ScanProgress
    , ScanStatus(..)
    , currentPart
    , getCurrentPartIndex
    , init
    , justFinishedAPart
    , scanComplete
    , scanDurationInMilliseconds
    )

import Time
import Utils.List as ListUtils
import Utils.Phrases as Phrases


type alias Model =
    { status : ScanStatus
    , duration : String
    , direction : Direction
    }


type ScanStatus
    = NotStartedScanning
    | CountingDown Int
    | Scanning ScanProgress PauseStatus
    | FinishedScanning


type alias ScanProgress =
    { startTime : Time.Posix
    , timeAtLastTick : Time.Posix
    }


type PauseStatus
    = Paused
    | Running


type Direction
    = FeetToHead
    | HeadToFeet


init : Model
init =
    { status = NotStartedScanning
    , duration = "5"
    , direction = FeetToHead
    }


getCurrentPartIndex : Time.Posix -> Time.Posix -> Model -> Int
getCurrentPartIndex startTime currentTime model =
    (Time.posixToMillis currentTime - Time.posixToMillis startTime)
        // millisecondsPerPart model


currentPart : Model -> String
currentPart model =
    let
        error =
            "Error: body part not found — this is a bug in the app!"
    in
    case model.status of
        Scanning { startTime, timeAtLastTick } _ ->
            let
                index =
                    getCurrentPartIndex startTime timeAtLastTick model
            in
            model
                |> parts
                |> ListUtils.getByIndex index
                |> Maybe.withDefault error

        _ ->
            error


millisecondsPerPart : Model -> Int
millisecondsPerPart model =
    (toFloat (scanDurationInMilliseconds model)
        / toFloat (partCount model)
    )
        |> floor


scanComplete : Int -> Model -> Bool
scanComplete currentPartIndex model =
    currentPartIndex >= partCount model


justFinishedAPart : ScanProgress -> Time.Posix -> Model -> Bool
justFinishedAPart { startTime, timeAtLastTick } currentTime model =
    let
        oldIndex =
            getCurrentPartIndex startTime timeAtLastTick model

        newIndex =
            getCurrentPartIndex startTime currentTime model
    in
    oldIndex < newIndex


scanDurationInMilliseconds : Model -> Int
scanDurationInMilliseconds model =
    model.duration
        |> String.toFloat
        |> Maybe.withDefault 5
        |> (*) (60 * 1000)
        |> round


parts : Model -> List String
parts model =
    case model.direction of
        HeadToFeet ->
            Phrases.bodyPartsHeadToFeet

        FeetToHead ->
            Phrases.bodyPartsFeetToHead


partCount : Model -> Int
partCount model =
    List.length (parts model)
