module Main.Actions exposing (Action(..))

import Main.Model exposing (Direction, PauseStatus, ScanProgress)
import Time


type Action
    = SelectDirection Direction
    | SetDuration String
    | RunScanCountdown Int
    | StartScan Time.Posix
    | PauseScan ScanProgress
    | UnpauseScan ScanProgress
    | StopScan
    | CheckTimer ScanProgress PauseStatus Time.Posix
    | Keypress String
