module Main exposing (main)

import Browser
import Main.Actions exposing (Action)
import Main.Model as Model exposing (Model)
import Main.Subscriptions exposing (subscriptions)
import Main.Update exposing (update)
import Main.View exposing (view)


main : Program String Model Action
main =
    Browser.document
        { init = \_ -> ( Model.init, Cmd.none )
        , view =
            \model ->
                { title = "Body Scan"
                , body = [ view model ]
                }
        , update = update
        , subscriptions = subscriptions
        }
