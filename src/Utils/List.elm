module Utils.List exposing (getByIndex)

import Array


getByIndex : Int -> List a -> Maybe a
getByIndex index list =
    list
        |> Array.fromList
        |> Array.get index
