module Utils.Phrases exposing
    ( bodyPartsFeetToHead
    , bodyPartsHeadToFeet
    , focusLinesWithYour
    , focusLinesWithThe
    )


bodyPartsFeetToHead : List String
bodyPartsFeetToHead =
    bodyParts
        ++ [ "whole head"
           , "whole body"
           ]


bodyPartsHeadToFeet : List String
bodyPartsHeadToFeet =
    List.reverse bodyParts
        ++ [ "whole body"
           ]


bodyParts : List String
bodyParts =
    [ "toes"
    , "feet"
    , "ankles"
    , "shins"
    , "calves"
    , "knees"
    , "back of your thighs"
    , "front of your thighs"
    , "butt"
    , "back"
    , "low back"
    , "mid back"
    , "upper back"
    , "groin"
    , "stomach"
    , "sternum"
    , "chest"
    , "hands"
    , "wrists"
    , "inside forearms"
    , "outside forearms"
    , "elbows"
    , "upper arms"
    , "shoulders"
    , "neck"
    , "jaw"
    , "cheeks"
    , "eyes"
    , "forehead"
    , "back of your head"
    , "top of your head"
    ]


focusLinesWithYour : List String
focusLinesWithYour =
    [ "move your focus to your"
    , "focus on your"
    , "and now your"
    ]


focusLinesWithThe : List String
focusLinesWithThe =
    [ "move your focus to the"
    , "focus on the"
    , "and now the"
    ]
