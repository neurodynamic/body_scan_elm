module Utils.Subscriptions exposing (keypress)

import Browser.Events
import Json.Decode as Decode exposing (Decoder, Error)


keypress : (String -> action) -> Sub action
keypress keypressAction =
    Decode.field "key" Decode.string
        |> Decode.map keypressAction
        |> Browser.Events.onKeyDown
