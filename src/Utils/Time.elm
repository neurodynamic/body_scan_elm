module Utils.Time exposing
    ( add
    , difference
    , differenceToTimeString
    , millisToTimeString
    )

import Time


differenceToTimeString : Time.Posix -> Time.Posix -> String
differenceToTimeString time1 time2 =
    difference time1 time2
        |> Time.posixToMillis
        |> millisToTimeString


difference : Time.Posix -> Time.Posix -> Time.Posix
difference time1 time2 =
    (Time.posixToMillis time2 - Time.posixToMillis time1)
        |> Time.millisToPosix


add : Time.Posix -> Time.Posix -> Time.Posix
add time1 time2 =
    (Time.posixToMillis time2 + Time.posixToMillis time1)
        |> Time.millisToPosix


millisToTimeString : Int -> String
millisToTimeString millis =
    let
        minutes =
            millis // (1000 * 60)

        seconds =
            (millis - (minutes * 1000 * 60)) // 1000
    in
    String.fromInt minutes ++ ":" ++ String.pad 2 '0' (String.fromInt seconds)
