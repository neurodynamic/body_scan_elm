import 'mil-no-sleep/mil-no-sleep.js'
import { Elm } from './src/Main.elm'
import NoSleep from 'nosleep.js'

const noSleep = new NoSleep()
let firefoxSleepLock = null
let chromeSleepLock = null

const app = Elm.Main.init({
  node: document.querySelector('main'),
  flags: "Body Scan"
})

app.ports.say.subscribe(phrase => {
  const msg = new SpeechSynthesisUtterance(phrase)
  window.speechSynthesis.speak(msg)
})

app.ports.allowSleeping.subscribe(_ => {
  noSleep.disable()
  firefoxAllowSleep()
  chromeAllowSleep()
})

app.ports.preventSleeping.subscribe(_ => {
  noSleep.enable()
  firefoxPreventSleep()
  chromePreventSleep()
})

function firefoxPreventSleep() {
  if (navigator.requestWakeLock) {
    firefoxSleepLock = navigator.requestWakeLock('screen')
    console.log('Firefox WakeLock is active')
  } else {
    console.log('Firefox WakeLock not available.')
  }
}

function firefoxAllowSleep() {
  if (navigator.requestWakeLock && firefoxSleepLock) {
    firefoxSleepLock.unlock()
    firefoxSleepLock = null
    console.log('Firefox WakeLock released.')
  }
}

function chromePreventSleep() {
  if (navigator.wakeLock) {
    try {
      chromeSleepLock = navigator.wakeLock.request('screen')
      chromeSleepLock.addEventListener('release', () => console.log('Chrome WakeLock was released'))
      console.log('Chrome WakeLock is active')
    } catch (err) {
      console.error(`Chrome WakeLock request failed: ${err.name}, ${err.message}`)
    }
  } else {
    console.log('Chrome WakeLock not available.')
  }
}

function chromeAllowSleep() {
  if (navigator.wakeLock && chromeSleepLock) {
    chromeSleepLock.release()
    chromeSleepLock = null
    console.log('Chrome WakeLock released.')
  }
}